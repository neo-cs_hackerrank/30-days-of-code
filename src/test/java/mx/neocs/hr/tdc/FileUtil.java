/*
 * Test.java
 * Created 05/may/2018 01:20:41 p. m.
 */

package mx.neocs.hr.tdc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class FileUtil {

    public static InputStream getInput(String pathFile) {
        return FileUtil.class.getClassLoader().getResourceAsStream(pathFile);
    }

    public static String getOutput(String pathFile) throws IOException {
        final StringBuilder sb = new StringBuilder();
        
        try (InputStream is = FileUtil.class.getClassLoader().getResourceAsStream(pathFile);
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr)) {
            br.lines().forEach( line -> {
                sb.append(line);
                sb.append(System.lineSeparator());
            });
        }

        return sb.toString();
    }
    
}
