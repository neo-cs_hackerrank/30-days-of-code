/*
 * Solution.java
 * Created 09/may/2018 09:07:20 p. m.
 */
package mx.neocs.hr.tdc.day1;

import java.util.Scanner;

/**
 * <h1>Day 1: Data Types</h1>
 * 
 * <h2>Objective</h2>
 * <p>Today, we're discussing data types.</p>
 * 
 * <h2>Task</h2>
 * <p>
 *    Complete the code in the editor below. The variables <em>i</em>,
 *    <em>d</em>, and <em>s</em> are already declared and initialized for you.
 *    You must:
 * </p>
 * <ol>
 *     <li>
 *         Declare <code>3</code> variables: one of type <code>int</code>, one
 *         of type <code>double</code>, and one of type <code>String</code>.
 *     </li>
 *     <li>
 *         Read <code>3</code> lines of input from stdin (according to the
 *         sequence given in the <em>Input Format</em> section below) and
 *         initialize your <code>3</code> variables.
 *     </li>
 *     <li>
 *         Use the <code>+</code> operator to perform the following operations:
 *         <ol>
 *             <li>
 *                 Print the sum of <em>i</em> plus your int variable on a new
 *                 line.
 *             </li>
 *             <li>
 *                 Print the sum of <em>d</em> plus your double variable to a
 *                 scale of one decimal place on a new line.
 *             </li>
 *             <li>
 *                 Concatenate <em>s</em> with the string you read as input and
 *                 print the result on a new line.
 *             </li>
 *         </ol>
 *     </li>
 * </ol>
 * 
 * <h3>Input Format</h3>
 * 
 * <p>The first line contains an integer that you must sum with <em>i</em>.</p> 
 * <p>The second line contains a double that you must sum with <em>d</em>.</p>
 * <p>
 *    The third line contains a string that you must concatenate with
 *    <em>s</em>.
 * </p>
 * 
 * <h3>Output Format</h3>
 * <p>
 *    Print the sum of both integers on the first line, the sum of both doubles
 *    (scaled to  decimal place) on the second line, and then the two
 *    concatenated strings on the third line.
 * </p>
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    /**
     * Read from the console an integer, a point floating number and a string;
     * and make additions with the numbers and a concatenation with the string.
     *   
     * @param args is not used.
     */
    public static void main(String[] args) {
        int i = 4;
        double d = 4.0;
        String s = "HackerRank ";
                
        Scanner scanner = new Scanner(System.in);
        int firstNumber = Integer.parseInt(scanner.nextLine());
        double secondNumber = Double.parseDouble(scanner.nextLine());
        String text = scanner.nextLine();
        scanner.close();

        System.out.println(i + firstNumber);
        System.out.println(d + secondNumber);
        System.out.println(s + text);
    }

}
