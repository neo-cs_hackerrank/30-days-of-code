/*
 * Person.java
 * Created 06/may/2018 01:08:13 p. m.
 */
package mx.neocs.hr.tdc.day4;

import java.text.MessageFormat;
import java.util.Scanner;

/**
 * <h1>Day 4: Class vs. Instance</h1>
 * <h2>Objective</h2>
 * <p>
 *    In this challenge, we're going to learn about the difference between a
 *    <em>class</em> and an <em>instance</em>; because this is an <em>Object
 *    Oriented</em> concept, it's only enabled in certain languages.
 * </p>
 * <h2>Task</h2> 
 * <p>
 *    Write a Person class with an instance variable, <code>age</code>, and a
 *    constructor that takes an integer, <code>initialAge</code>, as a
 *    parameter. The constructor must assign <code>initialAge</code> to
 *    <code>age</code> after confirming the argument passed as
 *    <code>initialAge</code> is not negative; if a negative argument is passed
 *    as <code>initialAge</code>, the constructor should set <code>age</code> to
 *    <code>0</code> and print <code>Age is not valid, setting age to 0.</code>.
 *    <br>
 *    In addition, you must write the following instance methods:
 * </p>
 * <ol>
 *     <li>{@link #yearPasses()} should increase the {@link #age} instance
 *         variable by <code>1</code>.</li>
 *     <li>{@link #amIOld()} should perform the following conditional actions:
 *         <ul>
 *             <li>If , print You are young..</li>
 *             <li>If  and , print <code>You are a teenager.</code>.</li>
 *             <li>Otherwise, print <code>You are old.</code>.</li>
 *         </ul>
 *     </li>
 * </ol>
 * 
 * <h3>Input Format</h3>
 * <p>Input is handled for you by the stub code in the editor.</p>
 * <p>
 *    The first line contains an integer, <em>T</em> (the number of test cases),
 *    and the <em>T</em> subsequent lines each contain an integer denoting the
 *    <code>age</code> of a Person instance.
 * </p>
 * <h3>Constraints</h3>
 * <ul>
 *     <li>1 ≤ <em>T</em> ≤ 4</li>
 *     <li>-5 ≤ <em>age</em> ≤ 30</li>
 * </ul>
 * 
 * <h3>Output Format</h3>
 * <p>
 *    Complete the method definitions provided in the editor so they meet
 *    the specifications outlined above; the code to test your work is already
 *    in the editor. If your methods are implemented correctly, each test case
 *    will print <code>2</code> or <code>3</code> lines (depending on whether
 *    or not a valid <code>initialAge</code> was passed to the constructor).
 * </p>
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Person {

    private static final String MESSAGE_TEMPLATE = "You are {0}."; 
    private static final String [] TEMPLATE_VALUES = {"young", "a teenager", "old"};  
    private int age;
    
    public Person(int initialAge) {
        // Add some more code to run some checks on initialAge
        if (initialAge < 0) {
            age = 0;
            System.out.println("Age is not valid, setting age to 0.");
        } else {
            age = initialAge;
        }
    }

    public void amIOld() {
        String message;
        // Write code determining if this person's age is old and print the correct statement:
        if (age < 13) {
            message = MessageFormat.format(MESSAGE_TEMPLATE, TEMPLATE_VALUES[0]);
        } else if (age >= 13 && age < 18) {
            message = MessageFormat.format(MESSAGE_TEMPLATE, TEMPLATE_VALUES[1]);
        } else {
            message = MessageFormat.format(MESSAGE_TEMPLATE, TEMPLATE_VALUES[2]);
        }

        System.out.println(message);
    }

    public void yearPasses() {
        // Increment this person's age.
        age++;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int age = sc.nextInt();
            Person p = new Person(age);
            p.amIOld();
            for (int j = 0; j < 3; j++) {
                p.yearPasses();
            }
            p.amIOld();
            System.out.println();
        }
        sc.close();
    }

}
