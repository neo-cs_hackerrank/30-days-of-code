/*
 * package-info.java
 * Created 06/may/2018 12:53:47 p. m.
 */

/**
 * Package for the solution for day 4.
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day4;
