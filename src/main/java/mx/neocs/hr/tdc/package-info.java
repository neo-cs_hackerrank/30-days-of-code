/*
 * package-info.java
 * Created 05/may/2018 02:18:15 p. m.
 */

/**
 * This is the main package with my solutions for the 30 Days of Code from 
 *  <a href="https://www.hackerrank.com/">HackerRank</a>.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc;
