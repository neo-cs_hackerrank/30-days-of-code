/*
 * Solution.java
 * Created 28/sep/2018 07:09:58 p. m.
 */
package mx.neocs.hr.tdc.day2;

import java.util.Scanner;

/**
 * <h1>Day 2: Operators</h1>
 * 
 * <h2>Objective</h2> 
 * <p>In this challenge, you'll work with arithmetic operators.</p>
 * 
 * <h2>Task</h2> 
 * <p>
 *    Given the meal price (base cost of a meal), tip percent (the percentage
 *    of the meal price being added as tip), and tax percent (the percentage of
 *    the meal price being added as tax) for a meal, find and print the meal's
 *    total cost.
 * </p>
 * 
 * <p>
 *    <strong>Note:</strong> Be sure to use precise values for your
 *    calculations, or you may end up with an incorrectly rounded result!
 * </p>
 * 
 * <h3>Input Format</h3>
 * 
 * <p>
 *    There are <em>3</em> lines of numeric input:<br/>
 *    The first line has a double, <em>mealCost</em> (the cost of the meal
 *    before tax and tip).<br/>
 *    The second line has an integer, <em>tipPercent</em> (the percentage of
 *    <em>mealCost</em> being added as tip).<br/>
 *    The third line has an integer, <em>taxPercent</em> (the percentage of
 *    <em>mealCost</em> being added as tax).
 * </p>
 * 
 * <h3>Output Format</h3>
 * <p>
 *    Print the total meal cost, where <em>totalCost</em> is the rounded integer
 *    result of the entire bill (<em>mealCost</em> with added tax and tip).
 * </p>
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    /**
     * Read three numbers from the console and make the calculation for get the
     * total meal cost.
     * 
     * @param args is not used.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double mealCost = in.nextDouble();
        int tipPercent = in.nextInt();
        int taxPercent = in.nextInt();
        in.close();

        double tip = mealCost * ((double) tipPercent / 100);
        double tax = mealCost * ((double) taxPercent / 100);
        double totalCost = mealCost + tip + tax;
        long cost = Math.round(totalCost);

        System.out.format("The total meal cost is %d dollars.", cost);
    }
}
