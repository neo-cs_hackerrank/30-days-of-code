/*
 * Solution.java
 * Created 11/may/2018 09:18:07 p. m.
 */
package mx.neocs.hr.tdc.day9;

//import java.io.BufferedWriter;
//import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    private static final Scanner scanner = new Scanner(System.in);
    
    // Complete the factorial function below.
    static int factorial(int n) {
        if (n > 0) {
            return n * factorial(n - 1); 
        } else {
            return 1;
        }
    }

    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int result = factorial(n);

//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();

//        bufferedWriter.close();
        System.out.println(result);

        scanner.close();

    }

}
