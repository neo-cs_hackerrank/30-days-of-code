/*
 * Solution.java
 * Created 05/may/2018 02:37:13 p. m.
 */

package mx.neocs.hr.tdc.day0;

import java.util.Scanner;

/**
 * <h1>Day 0: Hello, World.</h1>
 * 
 * <h2>Objective</h2>
 * <p>In this challenge, we review some basic concepts that will get you started
 *  with this series. You will need to use the same (or similar) syntax to read
 *  input and write output in challenges throughout
 *  <a href="https://www.hackerrank.com/">HackerRank</a>.</p>
 * 
 * <h2>Task</h2> 
 * <p>To complete this challenge, you must save a line of input from standard in
 *  to a variable, print <code>Hello, World.</code> on a single line, and
 *  finally print the value of your variable on a second line.</p>
 * 
 * <p>You've got this!</p>
 * 
 * <h3>Input Format</h3>
 * <p>A single line of text denoting <code>inputString</code> (the variable
 *  whose contents must be printed).</p>
 * 
 * <h3>Output Format</h3>
 * <p>Print <code>Hello, World.</code> on the first line, and the contents of
 *  <code>inputString</code> on the second
 *  line.</p>
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    /**
     * Read from the console a string and print "Hello, World." and the string
     * recived from the console.
     * @param args is not used.
     */
    public static void main(String[] args) {
        // Create a Scanner object to read input from standard input.
        Scanner scan = new Scanner(System.in);

        // Read a full line of input from standard input and save it to our
        // variable, inputString.
        String inputString = scan.nextLine();

        // Close the scanner object, because we've finished reading
        // all of the input from standard input needed for this challenge.
        scan.close();

        // Print a string literal saying "Hello, World." to standard output.
        System.out.println("Hello, World.");

        // Write a line of code here that prints the contents of inputString to
        // standard output.
        System.out.println(inputString);
    }
}
