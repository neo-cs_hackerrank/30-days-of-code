/*
 * package-info.java
 * Created 05/may/2018 02:18:15 p. m.
 */

/**
 * Package for the solution for day 0.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day0;
