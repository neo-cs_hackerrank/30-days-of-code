/*
 * Solution.java
 * Created 12/may/2018 11:48:00 p. m.
 */

package mx.neocs.hr.tdc.day10;

import java.util.Scanner;

/**
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 */
public class Solution {

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * @param args
     */
    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        scanner.close();
        
        char [] binary = Integer.toBinaryString(n).toCharArray();
        int count = 0;
        int last = 0;

        for(int i = 0; i < binary.length; i++) {
            if (binary[i] == '1') {
                count += 1;
                last = last > count ? last : count;
            } else {
                count = 0;
            }
        }
        
        System.out.println(last);
    }

}
