/*
 * package-info.java
 * Created 07/may/2018 10:24:27 p. m.
 */

/**
 * Package for the solution for day 5.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day5;
