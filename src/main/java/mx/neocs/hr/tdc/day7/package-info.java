/*
 * package-info.java
 * Created 09/may/2018 08:31:30 p. m.
 */

/**
 * Package for the solution for day 7.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day7;
