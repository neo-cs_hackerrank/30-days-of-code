/*
 * Solution.java
 * Created 09/may/2018 08:32:10 p. m.
 */
package mx.neocs.hr.tdc.day7;

import java.util.Scanner;

/**
 * <h1>Day 7: Arrays</h1>
 * 
 * <h2>Objective</h2>
 * <p>Today, we're learning about the Array data structure.</p>
 * 
 * <h2>Task</h2> 
 * <p>
 *    Given an array, <em>A</em>, of <em>N</em> integers, print <em>A</em>'s
 *    elements in <em>reverse</em> order as a single line of space-separated
 *    numbers.
 * </p>
 * 
 * <h3>Input Format</h3>
 * <p>
 *    The first line contains an integer, <em>N</em> (the size of our array).
 * </p> 
 * <p>
 *    The second line contains <em>N</em> space-separated integers describing
 *    array <em>A</em>'s elements.
 * </p>
 * 
 * <h3>Constraints</h3>
 * <ul>
 *     <li>1 ≤ <em>N</em> ≤ 1000</li>
 *     <li>
 *         1 ≤ <em>A<sub>i</sub></em> ≤ 10000, where <em>A<sub>i</sub></em> is 
 *         the <em>i<sup>th</sup></em> integer in the array.
 *     </li>
 * </ul>
 * 
 * <h3>Output Format</h3>
 * <p>
 *    Print the elements of array <em>A</em> in reverse order as a single line
 *    of space-separated numbers.
 * </p>
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 */
public class Solution {

    /**
     * Read from the console two lines, the first to set the array size and the
     * second the array and after print in console the same array but in reverse
     * order.
     *  
     * @param args is not used.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int i=0; i < n; i++){
            arr[i] = in.nextInt();
        }
        in.close();
        
        for(int i = arr.length - 1; i > -1; i--) {
            System.out.print(arr[i]);

            if (i > 0) {
                System.out.print(' ');
            }
        }
        
        System.out.println();
    }

}
